import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataState } from './enum/data-state.enum';
import { AppState } from './model/app-status';
import { OsService } from './os.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit{
  // title = 'project-test';

  // array = ['Text', 2, 'Text1', 'Text2'];

  // isTrue = true;

  // myLowerCase = 'abc-dec'

  // mydate = new Date();
   os : any;

  // public os ?: Os[];

  constructor(private osService: OsService){}

  ngOnInit() {
    this.osService.servers$
    .subscribe(response => {
      this.os = response;
      DataState.LOADING_STATE
    });

  
  }

}
