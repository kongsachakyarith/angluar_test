import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appCustomTextDirective]',
})
export class CustomTextDirectiveDirective {
  constructor(private elRef: ElementRef) {
    elRef.nativeElement.style.background = 'red';
  }
}
