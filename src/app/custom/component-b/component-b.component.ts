import { Component, OnInit } from '@angular/core';
import { PostsService } from 'src/app/os.service';

@Component({
  selector: 'app-component-b',
  templateUrl: './component-b.component.html',
  styleUrls: ['./component-b.component.css']
})
export class ComponentBComponent implements OnInit {

  constructor(
    private postsService: PostsService
  ) { }

  ngOnInit(): void {
    console.log(this.postsService.getPost());
    
  }

}
