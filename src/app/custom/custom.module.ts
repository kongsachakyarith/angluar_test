import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentBComponent } from './component-b/component-b.component';
import { ComponentCComponent } from './component-c/component-c.component';



@NgModule({
  declarations: [
    ComponentBComponent,
    ComponentCComponent
  ],
  exports:[ComponentBComponent,ComponentCComponent],
  imports: [
    CommonModule
  ]
})
export class CustomModule { }
