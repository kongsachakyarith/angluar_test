import { Status } from "../enum/status.enum";

export interface Os {
    id?: string;
    name: string;
    version: string;
    status : Status
}