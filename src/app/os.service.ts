import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Status } from './enum/status.enum';
import { Os } from './model/os';

@Injectable({
  providedIn: 'root'
})
export class OsService {
 
  private apiServerUrl = environment.apiBaseUrl;

  httpHeaderOptions={
    header : new HttpHeaders(
  {'Content-Type':'application/json','Accept':'application/json'})}

  constructor(private http: HttpClient) { }

 servers$ = <Observable<Os>> this.http.get<Os>(`${this.apiServerUrl}/os`).pipe(
  tap(console.log),
  catchError(this.handleError)
 );


private handleError(error: HttpErrorResponse): Observable<never> {
  console.log(error);
  return throwError(`An error occurred - Error code: ${error.status}`);
}

}
